# Who Is Viewing

## Overview

This utility bar Lightning Component allows the user to see all users who are currently viewing the same record detail page in real-time.

## Demo

[See the demo video here.](https://www.youtube.com/watch?v=j354o2isXBw)

## Screenshots

![Component Exposed from Utility Bar](media/component.png)

## Functional Overview

The component consists of an Aura Component wrapper ([whoIsViewingWrapper](force-app/main/default/aura/whoIsViewingWrapper)) and a nested Lightning Web Component ([whoIsViewing](force-app/main/default/lwc/whoIsViewing)). The wrapper is context-aware and passes the current page's record Id (or null if there is not one) to the LWC. Upon the record Id changing, the LWC will look at both the old and new values. 

If the old value was a record page, it will inform the controller to remove the user from the list of viewers and publish an event updating other users of the new list of viewers. Similarly, if the new value is a record page, the controller ([whoIsViewingController](force-app/main/default/classes/whoIsViewingController.cls)) will add the user to the list of viewers for that record and publish an event updating other users of the new list of viewers.

A garbage collector ([GarbageCollectorSchedulable](force-app/main/default/classes/GarbageCollectorSchedulable.cls)) runs periodically to account for any missed updates. For example, if a user's computer loses power suddenly it could possibly create an orphaned record.