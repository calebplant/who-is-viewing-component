import { LightningElement, api, wire } from 'lwc';
import userId from '@salesforce/user/Id';
import { subscribe, unsubscribe, onError, setDebugFlag, isEmpEnabled } from 'lightning/empApi';
import recordFocused from '@salesforce/apex/whoIsViewingController.recordFocused';
import recordUnfocused from '@salesforce/apex/whoIsViewingController.recordUnfocused';


export default class WhoIsViewing extends LightningElement {

    @api
    get focusedId() {
        return this._focusedId;
    }
    set focusedId(value) {
        let oldFocus = this._focusedId;
        this.setAttribute('focusedId', value);
        this._focusedId = value;
        this.handleFocusedIdChange(oldFocus);
    }
    channelName = '/event/Record_View_Update__e';
    subscription = {};
    recordViewers;

    // private
    _focusedId;

    // Lifecycle Hooks
    connectedCallback() {
        console.log('Initialize whoIsViewing.');
        this.registerErrorListener();
        this.subscribeToEventChannel();
        window.addEventListener('beforeunload', this.handleBeforeUnload.bind(this));
    }
    disconnectedCallback() {
        this.unsubscribeFromEventChannel();
    }

   subscribeToEventChannel() {
        const messageCallback = response => {
            console.log('New message received: ', JSON.stringify(response));       

            if(response.data.payload.Focused_Record_Id__c == this.focusedId) {
                console.log('Received viewers:');
                console.log(response.data.payload.Current_Viewers__c);
                
                this.recordViewers = response.data.payload.Current_Viewers__c.split(',');
            }
        };

        subscribe(this.channelName, -1, messageCallback).then(response => {
            console.log('Subscription request sent to: ', JSON.stringify(response.channel));
            this.subscription = response;
        });
    }
    unsubscribeFromEventChannel() {
        this.toggleSubscribeButton(false);

        unsubscribe(this.subscription, response => {
            console.log('unsubscribe() response: ', JSON.stringify(response));
        });
    }

    // Handlers
    handleFocusedIdChange(oldFocus) {
        console.log('whoIsViewing :: handleFocusedIdChange');
        console.log('New focus: ' + this._focusedId);
        if(this._focusedId) {
            recordFocused({focusedRecordId: this._focusedId, userId: userId})
            .then(result => {
                console.log('Successfully called focus');
            })
            .catch(error => {
                console.log('Error calling focus.');
                console.log(error);
            })
        } else {
            this.recordViewers = null;
        }
        if(oldFocus) {
            recordUnfocused({unfocusedRecordId: oldFocus, userId: userId})
            .then(result => {
                console.log('Successfully called unfocus');
            })
            .catch(error => {
                console.log('Error calling unfocus.');
                console.log(error);
            })
        }
    }

    handleEventReceived(response) {
        console.log('focusedId');
        console.log(this.focusedId); 
        console.log('response.data.payload.Focused_Record_Id__c');
        console.log(response.data.payload.Focused_Record_Id__c);            

        if(response.data.payload.Focused_Record_Id__c == this.focusedId) {
            console.log('Received viewers:');
            console.log(response.data.payload.Current_Viewers__c);
            
            this.recordViewers = response.data.payload.Current_Viewers__c;
        }
    }

    handleBeforeUnload(event) {
        console.log('Bollocks.');

        if(this.focusedId) {
            recordUnfocused({unfocusedRecordId: this.focusedId, userId: userId})
            .then(result => {
                console.log('Successfully called unfocus');
            })
            .catch(error => {
                console.log('Error calling unfocus.');
                console.log(error);
            })
        }
        
        console.log('After Bollocks.');
        return null;
        
    }

    // Utils
    registerErrorListener() {
        onError(error => {
            console.log('Received error from server: ', JSON.stringify(error));
        });
    }
}