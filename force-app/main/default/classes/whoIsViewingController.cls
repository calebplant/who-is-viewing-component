public without sharing class whoIsViewingController {
    
    @AuraEnabled
    public static void recordFocused(Id focusedRecordId, Id userId)
    {
        System.debug('START recordFocused');
        System.debug('Received Id: ' + focusedRecordId);
        System.debug('Originating user: ' + userId);

        // Log view
        Record_View__c newView = new Record_View__c();
        newView.Record_Id__c = focusedRecordId;
        newView.User__c = userId;
        insert newView;

        // Get updated list of current viewers
        List<Record_View__c> currentViewers = [SELECT Id, User__r.Name FROM Record_View__c
                                                WHERE Record_Id__c = :focusedRecordId];
        String viewersString = '';
        for(Record_View__c eachViewer : currentViewers) {
            viewersString += eachViewer.User__r.Name + ',';
        }
        if(viewersString.length() > 0) {
            viewersString = viewersString.substring(0, viewersString.length() - 1); //Remove trailing ","
        }

        // Publish event
        Record_View_Update__e updateView = new Record_View_Update__e();
        updateView.Focused_Record_Id__c = focusedRecordId;
        updateView.Current_Viewers__c = viewersString;
        EventBus.publish(updateView);
    }

    @AuraEnabled
    public static void recordUnfocused(Id unfocusedRecordId, Id userId)
    {
        System.debug('START recordUnfocused');
        System.debug('Received Id: ' + unfocusedRecordId); 
        System.debug('Originating user: ' + userId);

        // Remove view
        List<Record_View__c> viewToDelete = [SELECT Id FROM Record_View__c
                                            WHERE Record_Id__c = :unfocusedRecordId
                                            AND User__c = :userId];
        delete viewToDelete;

        // Get updated list of current viewers
        List<Record_View__c> currentViewers = [SELECT Id, User__r.Name FROM Record_View__c
                                                WHERE Record_Id__c = :unfocusedRecordId];
        String viewersString = '';
        for(Record_View__c eachViewer : currentViewers) {
            viewersString += eachViewer.User__r.Name + ',';
        }
        if(viewersString.length() > 0) {
            viewersString = viewersString.substring(0, viewersString.length() - 1); //Remove trailing ","
        }

        // Publish event
        Record_View_Update__e updateView = new Record_View_Update__e();
        updateView.Focused_Record_Id__c = unfocusedRecordId;
        updateView.Current_Viewers__c = viewersString;
        EventBus.publish(updateView);
    }

    // @AuraEnabled
    // public static String testerFunc(String someString) {
    //     System.debug('START testerFunc' + someString);
    //     System.debug('END testerFunc' + someString + 'blabla');
    //     return 'Hello!';
    // }

    public static void clearStaleViews(Integer timeoutMinutes) {
            Datetime minCreateTime = System.now().addMinutes(-1 * timeoutMinutes);
            List<Record_View__c> staleViews = [SELECT Id FROM Record_View__c WHERE CreatedDate < :minCreateTime];
            // Delete orphaned views
            if(staleViews.size() > 0) {
                List<Database.DeleteResult> deleteResults = Database.delete(staleViews, false);
                for(Database.DeleteResult eachDR : deleteResults) {
                    if(!eachDr.isSuccess()) {
                        for(Database.Error err : eachDR.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        }
                    }
                }
            }
        }
}
