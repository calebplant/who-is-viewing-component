@isTest
public with sharing class whoIsViewingController_Test {

    private static String USER_ONE_NAME = 'Josh Brown';
    private static String USER_TWO_NAME = 'Steve Testerman';
    private static String ACCOUNT_ONE_NAME = 'Bananas';
    private static String ACCOUNT_TWO_NAME = 'Apples';

    @TestSetup
    static void makeData()
    {
        User userOne = [SELECT Id, Name FROM User WHERE Name = :USER_ONE_NAME];
        User userTwo = [SELECT Id, Name FROM User WHERE Name = :USER_TWO_NAME];
        Account accOne = new Account(Name=ACCOUNT_ONE_NAME);
        insert accOne;
        // ACcount accTwo = new Account(Name=ACCOUNT_TWO_NAME);
        // insert accTwo;
        List<Record_View__c> recordViews = new List<Record_View__c>();
        recordViews.add(new Record_View__c(Record_Id__c=accOne.Id, User__c=userOne.Id));
        recordViews.add(new Record_View__c(Record_Id__c=accOne.Id, User__c=userTwo.Id));

        insert recordViews;
    }

    @isTest
    static void doesRecordFocusedAddNewViewer()
    {
        Account accOne = [SELECT Id FROM Account WHERE Name = :ACCOUNT_ONE_NAME];
        User userTwo = [SELECT Id, Name FROM User WHERE Name = :USER_TWO_NAME];
        Integer startSize = [SELECT COUNT() FROM Record_View__c];

        Test.startTest();
        whoIsViewingController.recordFocused(accOne.Id, userTwo.Id);
        Test.stopTest();

        Integer endSize = [SELECT COUNT() FROM Record_View__c];

        System.assert(endSize == startSize+1);
    }

    @isTest
    static void doesRecordUnfocusedRemoveViewer()
    {
        Account accOne = [SELECT Id FROM Account WHERE Name = :ACCOUNT_ONE_NAME];
        User userOne = [SELECT Id, Name FROM User WHERE Name = :USER_ONE_NAME];
        Integer startSize = [SELECT COUNT() FROM Record_View__c]; 
                            // WHERE Record_Id__c = :accOne.Id
                            // AND User__c = :userOne.Id];

        Test.startTest();
        whoIsViewingController.recordUnfocused(accOne.Id, userOne.Id);
        Test.stopTest();

        Integer endSize = [SELECT COUNT() FROM Record_View__c];
                            // WHERE Record_Id__c = :accOne.Id
                            // AND User__c = :userOne.Id];

        System.assert(endSize == startSize-1);
    }

    @isTest
    static void doesClearStaleViewsRemoveExpiredViews()
    {
        Account accOne = [SELECT Id FROM Account WHERE Name = :ACCOUNT_ONE_NAME];
        User userTwo = [SELECT Id, Name FROM User WHERE Name = :USER_TWO_NAME];
        Datetime fiveHoursAgo = System.now().addHours(-5);

        Record_View__c staleView = new Record_View__c(Record_Id__c=accOne.Id, User__c=userTwo.Id);
        insert staleView;
        Integer startSize = [SELECT COUNT() FROM Record_View__c];
        Test.setCreatedDate(staleView.Id, fiveHoursAgo);

        Test.startTest();
        whoIsViewingController.clearStaleViews(30);
        Test.stopTest();

        List<Record_View__c> tester = [SELECT Id, Record_Id__c, User__r.Name FROM Record_View__c];
        System.debug(tester);
        Integer endSize = [SELECT COUNT() FROM Record_View__c];
        System.assert(endSize == startSize-1); 
    }
}
