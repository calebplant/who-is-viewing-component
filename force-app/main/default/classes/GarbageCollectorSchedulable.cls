global without sharing class GarbageCollectorSchedulable implements Schedulable{

    // Timeout in MINUTES
    private static Integer RECORD_VIEWERS_TIMEOUT = 30;
    
    global void execute(SchedulableContext sc) {
        whoIsViewingController.clearStaleComments(RECORD_VIEWERS_TIMEOUT);
    }
}
